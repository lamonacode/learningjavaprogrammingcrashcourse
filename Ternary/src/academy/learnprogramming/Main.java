package academy.learnprogramming;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("what day is it?");
        String day = scanner.nextLine();
        String howToSpendTime;

//        if (day.equalsIgnoreCase("saturday")){
//            howToSpendTime = "Relax";
//        } else {
//            howToSpendTime = "Work";
//        }

        howToSpendTime = day.equalsIgnoreCase("saturday") ? "Relax" : "Work";
        System.out.printf("%s on %s %n", howToSpendTime, day);
        scanner.close();
    }
}
