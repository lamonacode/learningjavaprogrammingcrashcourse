package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {
//                           0123456789012345678901234567890123456
//        String courseName = "Learn C# for Beginners Crash Course";
        StringBuilder courseName = new StringBuilder("Learn C# for Beginners Crash Course");
        String message = "Welcome to ";

//        String fullMessage = message + courseName;
//        System.out.println(fullMessage);
//
//        String fullMessage2 = String.format("%s%s", message,courseName);
//        System.out.println(fullMessage2);
//
//        System.out.printf("Hello, and %s the %s.%n", message,courseName);
//
//        for (int i = 0; i < courseName.length(); i++){
//            char character = courseName.charAt(i);
//            System.out.println(character);
//        }
//        System.out.println(courseName.charAt(6));
//
//        for (int i = 15; i < 23; i ++) {
//            System.out.print(courseName.charAt(i));
//        }
//        System.out.println();

        int position = 0;
//        int position = indexOfIgnoreCase(courseName, " j");
//        int position = courseName.lastIndexOf("C");
//        position = indexOfIgnoreCase(courseName, " c", 0);
//        System.out.println(position);
//
//        position = indexOfIgnoreCase(courseName, " c", position + 1);
//        System.out.println(position);
        do {
//            position = indexOfIgnoreCase(courseName, " c", position + 1);
            position = courseName.toString().toLowerCase().indexOf(" c".toLowerCase(), position + 1);
//            System.out.println(position);
            if (position != -1) {
                replaceByIndex(courseName, position, " c".length(), " Java");
                System.out.println(courseName);
            }

        } while (position != -1);

        String fixedName = courseName.toString().replace(" Java", " C");
        System.out.println(fixedName);
    }

    private static String replaceByIndex(String original, int start, int length, String replacement) {
        String toRemove = original.substring(start, start + length);
        return original.replaceFirst(toRemove, replacement);
    }

    private static StringBuilder replaceByIndex(StringBuilder original, int start, int length, String replacement) {
//        String toRemove = original.substring(start, start + length);
        return original.replace(start, start + length, replacement);
    }

//    private static int indexOfIgnoreCase(String text, String searchText, int fromIndex){
//        String textLower = text.toLowerCase();
//        String searchTextLower = searchText.toLowerCase();
//        return textLower.indexOf(searchTextLower, fromIndex);
//    }

    private static int indexOfIgnoreCase(String text, String searchText, int fromIndex){
        return text.toLowerCase().indexOf(searchText.toLowerCase(), fromIndex);
    }
}
