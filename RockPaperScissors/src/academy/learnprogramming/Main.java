package academy.learnprogramming;

import java.util.Random;
import java.util.Scanner;

public class Main {

    private static final int ROCK = 0;      // beats SCISSORS.      (SCISSORS + 1) %3 = 0
    private static final int PAPER = 1;     // beats ROCK.          (ROCK + 1) %3 = 1
    private static final int SCISSORS = 2;  // beats PAPER.         (PAPER +1) %3 = 2
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Random randomGenerator = new Random();

        String playerChoice;
        int playerValue;

        System.out.println("Please enter rock, paper or scissors");

        do {
            playerChoice = scanner.nextLine().toLowerCase();
            int computerValue = randomGenerator.nextInt(3);

            if (playerChoice.equalsIgnoreCase("rock")) {
                playerValue = ROCK;
            } else if (playerChoice.equalsIgnoreCase("paper")) {
                playerValue = PAPER;
            } else if (playerChoice.equalsIgnoreCase("scissors")) {
                playerValue = SCISSORS;
            } else if (playerChoice.equalsIgnoreCase("q")) {
                playerValue = -1;
            } else {
                System.out.printf("%s is not a valid choice%n", playerChoice);
                playerValue = 99;
            }

            if (playerValue >= 0) {
                if (playerValue < 99) {
                    String winner = determineWinner(playerValue, computerValue);
                    System.out.printf("Player chose %s, the computer has %s. %s%n",
                            getChoice(playerValue), getChoice(computerValue), winner);
                }
                System.out.println("Play Again, or enter 'q' to Quit.");
            }

        } while (playerValue >= 0);

        scanner.close();
    }

    public static boolean getYesNo(String aQuestion){
        System.out.println(aQuestion);
        String answer = scanner.nextLine();

        return answer.equalsIgnoreCase("yes");
    }

    public static String determineWinner(int aPlayer, int aComputer) {
        if (aComputer == aPlayer) {
            return "It's a tie!";
        } else if (aPlayer == ((aComputer + 1) %3)){
//        } else if (((aComputer == 0) && (aPlayer == 2)) ||
//                ((aComputer == 1) && (aPlayer == 0)) ||
//                ((aComputer == 2) && (aPlayer == 1))) {
            return "Player wins!";
        } else {
            return "Computer wins!";
        }
    }

    public static String getChoice(int aValue) {
        if (aValue == 0) {
            return "Rock";
        } else if (aValue == 1) {
            return "Paper";
        } else if (aValue == 2) {
            return "Scissors";
        } else return "ERROR";
    }

}
