import java.util.ArrayList;


/**
 * Customer
 */
public class Customer {

  private String name;
  private ArrayList<Double> transactions;

  public Customer(String name, Double amount) {
    this(name);
    this.transactions = new ArrayList<Double>();
    addTransaction(amount);
  }

  public Customer(String name) {
    this.name = name;
    this.transactions = new ArrayList<Double>();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ArrayList<Double> getTransactionList() {
    return transactions;
  }

  public void printTransactions(Customer customer) {
    double iTotal = 0.0;
    System.out.println("Transaction List for " + customer.getName()+ ": ");
    for (int i = 0; i < customer.transactions.size(); i++) {
      iTotal += customer.transactions.get(i);
      System.out.println(i+1 + ". " + customer.transactions.get(i));
      System.out.println("          TOTAL BAL:" + iTotal);
    }
  }

  public void setTransactionList(ArrayList<Double> transactions) {
    this.transactions = transactions;
  }

  public void addTransaction (Double amount) {
    this.transactions.add(amount);
  }

}
