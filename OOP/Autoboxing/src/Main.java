import java.util.Scanner;

/**
 * Main
 */
public class Main {
  public static Scanner scanner = new Scanner(System.in);
  public static Bank bank = new Bank();

  public static void main(String[] args) throws Exception {
    boolean quit = false;
    int choice = 0;
    printInstructions();

    while (!quit) {
      System.out.println("Enter choice: ");
      if (scanner.hasNextInt()) {
        choice = scanner.nextInt();
        scanner.nextLine();
      } else {
        System.out.println("Please enter an integer");
        scanner.nextLine();
        continue;
      }

      switch (choice) {
        case 0:
          printInstructions();
          break;
        case 2:
          printBranches();
          break;
        case 1:
          addBranch();
          break;
        case 4:
          printCustomers();
          break;
        case 3:
          addCustomer();
          break;
        case 6:
          printTransactions();
          break;
        case 5:
          addTransaction();
          break;
        case 7:
          quit = true;
          break;

      }
    }
  }

  public static void addBranch() {
    bank.addNewBranch(bank);
  }

  public static Branch findBranch() {
    System.out.println("Please enter the branch name: ");
    String name = scanner.nextLine();
    Branch branch = bank.findBranch(name);
    if (branch == null) {
      System.out.println(name + " is an NOT existing branch.");
    }
    return branch;
  }

  public static void addCustomer() {
    Branch branch = findBranch();
    if (branch != null) {
      branch.addNewCustomer();
    }
  }

  public static void printCustomers() {
    Branch branch = findBranch();
    if (branch != null) {
      branch.printCustomerList(branch);
    }
  }

  public static void addTransaction() {
    Branch branch = findBranch();
    if (branch != null) {
      branch.addCustomerTransaction(branch);
    }
  }

  public static void printTransactions() {
    Branch branch = findBranch();
    if (branch != null) {
      Customer customer = branch.findCustomer();
      if (customer != null) {
        customer.printTransactions(customer);
      }
    }
  }

  public static void printBranches() {
    System.out.println("Branch List: ");

    if (bank.branchList.size() == 0) {
      System.out.println("No Branches.");
      return;
    }
    for (int i = 0; i < bank.branchList.size(); i++) {
      System.out.println(i+1 + ". " + bank.branchList.get(i).getBranchName());
    }
  }

  public static void printInstructions() {
    System.out.println("\nPress ");
    System.out.println("\t 0 - To print options");
    System.out.println("\t 1 - To add a NEW branch");
    System.out.println("\t 2 - To print the list of Branches");
    System.out.println("\t 3 - To add a NEW Customer");
    System.out.println("\t 4 - To print a list of Customers");
    System.out.println("\t 5 - To add a Transaction");
    System.out.println("\t 6 - To print a List of Transactions");
    System.out.println("\t 7 - To exit");
  }

}
