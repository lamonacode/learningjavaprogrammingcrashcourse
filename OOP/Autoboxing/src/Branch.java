import java.util.ArrayList;
import java.util.Scanner;


/**
 * Branch
 */
public class Branch {

  public static Scanner scanner = new Scanner(System.in);

  private String branchName;
  private ArrayList<Customer> customerList;

  public Branch(String branchName) {
    this.branchName = branchName;
    this.customerList = new ArrayList<Customer>();
  }

  public ArrayList<Customer> getCustomerList() {
    return customerList;
  }

  public void setCustomerList(ArrayList<Customer> customerList) {
    this.customerList = customerList;
  }

  public Customer findCustomer() {
    System.out.println("Please enter customer's name: ");
    String name = scanner.nextLine();
    return findCustomer(name);
  }

  public Customer findCustomer(String name) {
    for (int i = 0; i < customerList.size(); i++) {
      Customer customer = customerList.get(i);
      if (customer.getName().equals(name)){
        return customer;
      }
    }
    return null;
  }

  public void addNewCustomer(){
    System.out.println("Please enter customer's name: ");
    String name = scanner.nextLine();
    while (findCustomer(name) != null) {
      System.out.println(name + " is an existing member. Please choose another name:");
      name = scanner.nextLine();
    }
    System.out.println("Please enter " + name + "'s beginning balance: ");
    Double amount = scanner.nextDouble();
    Customer customer = new Customer(name, amount);
    customerList.add(customer);
    System.out.println("Added " + name + ". Balance = " + amount);
  }

  public void addCustomerTransaction(Branch branch){
    System.out.println("Please enter customer's name: ");
    String name = scanner.nextLine();
    Customer customer = branch.findCustomer(name);
    if (customer == null) {
      System.out.println("Member not found. Unable to complete transaction.");
      return;
    }
    System.out.println("Please enter transaction amount: ");
    Double amount = scanner.nextDouble();
    scanner.nextLine();
    customer.addTransaction(amount);
    System.out.println("Transaction complete.");
  }

  public void printCustomerList(Branch branch) {
    System.out.println("Customer List for " + branch.getBranchName() + ": ");
    if (customerList.size() == 0) {
      System.out.println("No customers.");
      return;
    }
    for (int i = 0; i < customerList.size(); i++) {
      System.out.println(i+1 + ". " + customerList.get(i).getName());
    }
  }

  public String getBranchName() {
    return branchName;
  }

  public void setBranchName(String branchName) {
    this.branchName = branchName;
  }
}
