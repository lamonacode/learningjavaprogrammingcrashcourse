import java.util.ArrayList;
import java.util.Scanner;

/**
 * Bank
 */
public class Bank {
  public static Scanner scanner = new Scanner(System.in);

  private String bankName;
  public ArrayList<Branch> branchList;

  public Bank() {
    this.bankName = "myBank";
    this.branchList = new ArrayList<Branch>();
  }

  public void addNewBranch(Bank bank) {
    System.out.println("Please enter a name: ");
    String name = scanner.nextLine();
    while (findBranch(name) != null) {
      System.out.println(name + " is an existing branch. Please choose another name:");
      name = scanner.nextLine();
    }

    Branch branch = new Branch(name);
    bank.branchList.add(branch);
    System.out.println("Added " + branch.getBranchName());
  }

  public Branch findBranch(String name) {
    for (int i = 0; i < branchList.size(); i++) {
      Branch branch = branchList.get(i);
      if (branch.getBranchName().equals(name)){
        return branch;
      }
    }
    return null;
  }

  public String getBankName() {
    return bankName;
  }

  public void setBankName(String bankName) {
    this.bankName = bankName;
  }

  public ArrayList<Branch> getBranchList() {
    return branchList;
  }

  public void setBranchList(ArrayList<Branch> branchList) {
    this.branchList = branchList;
  }
}
