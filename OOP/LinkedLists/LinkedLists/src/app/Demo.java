package app;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Scanner;
import java.util.Iterator;

/**
 * Demo
 */
public class Demo {

  public static void main(String[] args) {
    LinkedList<String> places = new LinkedList<String>();
    // places.add("Rochester");
    // places.add("Buffalo");
    // places.add("D.C.");
    // places.add("NYC");
    // places.add("New Orleans");
    // places.add("Long Island");

    // printList(places);

    // places.add(1, "Toronto");
    // printList(places);

    // places.remove(4);
    // printList(places);

    addInOrder(places,"Rochester");
    addInOrder(places,"Buffalo");
    addInOrder(places,"D.C.");
    addInOrder(places,"NYC");
    addInOrder(places,"New Orleans");
    addInOrder(places,"Long Island");
    printList(places);
    visit(places);
  }

  private static void visit(LinkedList<String> myList){
    Scanner scanner = new Scanner(System.in);
    boolean quit = false;
    ListIterator<String> i = myList.listIterator();

    if (myList.isEmpty()) {
      System.out.println("Empty list");
      scanner.close();
      return;
    } else {
      System.out.println("Now visiting " + i.next());
      printMenu();
    }

    while(!quit) {
      int action = scanner.nextInt();
      scanner.nextLine();
      switch (action) {
        case 0:
          System.out.println("Vacation over");
          quit = true;
          break;
        case 1:
          if (i.hasNext()) {
            System.out.println("Now visiting " + i.next());
          } else {
            System.out.println("End of list");
          }
          break;
        case 2:
          if (i.hasPrevious()) {
            System.out.println("Now visiting " + i.previous());
          } else {
            System.out.println("Now at beginning of list");
          }
          break;
        case 3:
          printMenu();
          break;
      }
    }
    scanner.close();
  }

  private static void printMenu() {
    System.out.println("Please choose: ");
    System.out.println("0 - quit");
    System.out.println("1 - next city");
    System.out.println("2 - prev city");
    System.out.println("3 - print menu");
  }

  private static boolean addInOrder(LinkedList<String> myList, String city) {
    ListIterator<String> i = myList.listIterator();

    while(i.hasNext()){
      int compare = i.next().compareTo(city);
      if (compare == 0) {
        System.out.println(city + " already exists in the list.");
        return false;
      } else if (compare > 0) {
        myList.add(i.previousIndex(), city);
        return true;
      }
    }
    myList.add(city);
    return true;
  }

  private static void printList(LinkedList<String> myList) {
    Iterator<String> i = myList.iterator();
    while(i.hasNext()) {
      System.out.println(i.next());
    }
    System.out.println("--------------------------");
  }
}
