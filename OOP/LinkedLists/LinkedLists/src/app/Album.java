package app;

import java.util.ArrayList;


/**
 * Album
 */
public class Album {

  private ArrayList<Song> album = new ArrayList<Song>();

  public Album(ArrayList<Song> album) {
    this.album = album;
  }

  public ArrayList<Song> getAlbum() {
    return album;
  }

  public void setAlbum(ArrayList<Song> album) {
    this.album = album;
  }

  public Album() {
  }

  public void addSong(Song song){
    album.add(song);
  }

  public void addSong(String title, Double duration){
    if (findSong(title) == null) {
      album.add(new Song(title, duration));
    }
  }

  public Song findSong(String title) {
    for(Song song: this.album){
      if (song.getTitle().equals(title)){
        return song;
      }
    }
    return null;
    // Iterator<Song> i = album.iterator();
    // int index = 0;
    // while(i.hasNext()){
    //   int compare = i.next().getTitle().compareTo(title);
    //   if (compare == 0) {
    //     return album.get(index);
    //   }
    //   index ++;
    // }
    // return null;
  }
}
