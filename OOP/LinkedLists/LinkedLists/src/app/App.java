package app;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Scanner;
import java.util.Iterator;


public class App {
    public static void main(String[] args) throws Exception {
      LinkedList<Song> playlist = new LinkedList<Song>();

      Album soad = new Album();
      soad.addSong("Suite Pea", 3.45);
      soad.addSong("Toxicity", 4.13);
      soad.addSong("Prison", 5.19);
      soad.addSong("Sugar", 4.04);

      Album tool = new Album();
      tool.addSong("Stinkfist", 4.49);
      tool.addSong("Undertow", 4.59);
      tool.addSong("Descending", 14.49);
      tool.addSong("Lateralus", 8.53);
      tool.addSong("46 & 2", 6.03);

      addToPlaylist(playlist, tool, "Stinkfist");
      addToPlaylist(playlist, soad, "Sugar");
      addToPlaylist(playlist, tool, "Lateralus");
      addToPlaylist(playlist, soad, "Prison");

      // printMenu();
      playSongs(playlist);
    }

    private static void addToPlaylist(LinkedList<Song> playlist, Album album, String title) {
      Song aSong = album.findSong(title);
      if (aSong != null) {
        playlist.add(aSong);
      }
    }

    private static void playSongs(LinkedList<Song> playlist) {
      Scanner scanner = new Scanner(System.in);
      boolean quit = false;
      boolean fwd = true;
      ListIterator<Song> i = playlist.listIterator();

      if (playlist.isEmpty()) {
        System.out.println("No Songs");
        scanner.close();
        return;
      } else {
        System.out.println("Now playing " + i.next().getTitle());
        printMenu();
      }

      while(!quit) {
        int action = scanner.nextInt();

        scanner.nextLine();
        switch (action) {
          case 0:
            System.out.println("Powering down");
            quit = true;
            break;
          case 1:
            if (!fwd) {
              if (i.hasNext()) {
                i.next();
              }
              fwd = true;
            }
            if (i.hasNext()) {
              System.out.println("Now playing " + i.next().getTitle());
            } else {
              System.out.println("End of list");
              fwd = false;
            }
            break;
          case 2:
            if (fwd) {
              if (i.hasPrevious()) {
                i.previous();
              }
              fwd = false;
            }
            if (i.hasPrevious()) {
              System.out.println("Now playing " + i.previous().getTitle());
            } else {
              System.out.println("Beginning of list");
              fwd = true;
            }
            break;
          case 3:
            System.out.println("Now playing " + playlist.get(i.nextIndex() - 1).getTitle());
            break;
          case 4:
            System.out.println("Removed " + playlist.get(i.nextIndex() - 1).getTitle());
            i.remove();
            break;
            case 5:
            printPlaylist(playlist);
            break;
          case 6:
            printMenu();
            break;

        }
      }
      scanner.close();
    }

    private static void printPlaylist(LinkedList<Song> playlist) {
      Iterator<Song> i = playlist.iterator();
      System.out.println("_________________________________");
      while(i.hasNext()) {
        System.out.println(i.next());
      }
      System.out.println("_________________________________");
    }
    private static void printMenu() {
      System.out.println("Please choose: ");
      System.out.println("0 - quit");
      System.out.println("1 - Forward");
      System.out.println("2 - Back");
      System.out.println("3 - Repeat");
      System.out.println("4 - Remove Song");
      System.out.println("5 - Print List");
      System.out.println("6 - Menu");
    }


}
