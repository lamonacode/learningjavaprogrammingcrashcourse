import java.util.ArrayList;

/**
 * ContactList
 */
public class MobilePhone {

  private ArrayList<Contact> contactList = new ArrayList<Contact>();

  public void addItem(Contact item) {
    contactList.add(item);
  }

  public void printList() {
    for (int i = 0; i < contactList.size(); i++) {
      System.out.println((i+1) + ". " + contactList.get(i).getName() + ": " + contactList.get(i).getNumber());
    }
  }

  public void modifyList(String name, Contact newItem) {
    int pos = findItem(name);
    if (pos >= 0) {
      modifyList(pos, newItem);
    }
  }

  private void modifyList(int position, Contact newItem) {
    contactList.set(position, newItem);
  }

  private void removeListItem(int position) {
    contactList.remove(position);
  }

  public void removeListItem(String item) {
    int pos = findItem(item);
    if (pos >= 0) {
      removeListItem(pos);
    }
  }

  public int findItem(String searchItem) {
    int retItem = -1;
    String contactName = "";
    for (int i = 0; i < contactList.size(); i++){
      contactName = contactList.get(i).getName();
      if (contactName.equals(searchItem)) {
        retItem = i;
        break;
      }
    }
    return retItem;
  }

  public String getPhone(String name) {
    Contact contact = contactList.get(findItem(name));
    if (contact != null) {
      return contact.getNumber();
    }
    return "";
  }

  public ArrayList<Contact> getContactList() {
    return contactList;
  }

  public void setContactList(ArrayList<Contact> contactList) {
    this.contactList = contactList;
  }


}
