

import java.util.Scanner;

/**
 * Main
 */
public class Main {

  public static Scanner scanner = new Scanner(System.in);

  public static MobilePhone myPhone = new MobilePhone();


  // public static GroceryList groceryList = new GroceryList();
  public static void main(String[] args) {
    boolean quit = false;
    int choice = 0;
    printInstructions();

    while (!quit) {
      System.out.println("Enter choice: ");
      if (scanner.hasNextInt()) {
        choice = scanner.nextInt();
        scanner.nextLine();
      } else {
        System.out.println("Please enter an integer");
        scanner.nextLine();
        continue;
      }


      switch (choice) {
        case 0:
          printInstructions();
          break;
        case 1:
          printList();
          break;
        case 2:
          addItem();
          break;
        case 3:
          modifyItem();
          break;
        case 4:
          removeItem();
          break;
        case 5:
          searchForItem();
          break;
        case 6:
          quit = true;
          break;

      }
    }

  }

  public static void printInstructions() {
    System.out.println("\nPress ");
    System.out.println("\t 0 - To print options");
    System.out.println("\t 1 - To print the list");
    System.out.println("\t 2 - To add an item to the list");
    System.out.println("\t 3 - To modify an item");
    System.out.println("\t 4 - To remove an item");
    System.out.println("\t 5 - To find an item");
    System.out.println("\t 6 - To exit");
  }

  public static void printList() {
    System.out.println("Contact List: ");
    myPhone.printList();
  }

  public static void addItem() {
    System.out.println("Please enter a name: ");
    String name = scanner.nextLine();
    while (myPhone.findItem(name) >= 0) {
      System.out.println(name + " is an existing contact. Please choose another name:");
      name = scanner.nextLine();
    }
    System.out.println("Please enter " + name + "'s phone number: '");
    String number = scanner.nextLine();
    // Contact newContact = new Contact(name, number);
    myPhone.addItem(Contact.createNewContact(name, number));
  }

  public static void modifyItem(){
    System.out.println("Please enter the contact to modify: ");
    String name = scanner.nextLine();

    System.out.println("Please enter the new contact name or <Enter> to leave unchanged: ");
    String newName;
    if ((newName = scanner.nextLine()).isEmpty()) {
      newName = name;
    } else {
      while (myPhone.findItem(newName) >= 0) {
        System.out.println(newName + " is an existing contact. Please choose another name:");
        newName = scanner.nextLine();
      }
    }

    System.out.println("Please enter the new number or <Enter> to leave unchanged: ");
    String newNumber;
    if ((newNumber = scanner.nextLine()).isEmpty()) {
      newNumber = myPhone.getPhone(name);
    }

    Contact newContact = new Contact(newName, newNumber);
    myPhone.modifyList(name, newContact);
  }

  public static void removeItem() {
    System.out.println("Please enter the contact to remove: ");
    myPhone.removeListItem(scanner.nextLine());
  }

  public static void searchForItem() {
    System.out.println("Please enter the contact to look for: ");
    String searchItem = scanner.nextLine();
    if (myPhone.findItem(searchItem) >= 0) {
      System.out.println(searchItem + " found");
    } else {
      System.out.println(searchItem + " is not in the list");
    }
  }

  // public static void processArrayList() {
  //   ArrayList<String> newArray = new ArrayList<String>();
  //   newArray.addAll(groceryList.getGroceryList());

  //   ArrayList<String> nextArray = new ArrayList<String>(groceryList.getGroceryList());

  //   String[] myArray = new String[groceryList.getGroceryList().size()];
  //   myArray = groceryList.getGroceryList().toArray(myArray);
  // }

  // public static void reverse(int[] array) {
  //   int[] newArray = new int[array.length];
  //   int j = 0;
  //   for (int i = array.length - 1; i >= 0; i--){
  //     newArray[j] = array[i];
  //     j++;
  //   }
  //   System.out.println(Arrays.toString(newArray));
  // }

  // public static int[] sortArray(int[] array) {
  //   // Arrays.sort(array);
  //   // return array;
  //   int[] sortedArray = new int[array.length];

  //   for (int i = 0; i < array.length; i++){
  //     sortedArray[i] = array[i];
  //   }

  //   boolean flag = true;
  //   int temp;
  //   while (flag) {
  //     flag = false;
  //     for (int i = 0; i < sortedArray.length - 1; i++){
  //       if (sortedArray[i] < sortedArray[i+1]) {
  //         temp = sortedArray[i];
  //         sortedArray[i] = sortedArray[i+1];
  //         sortedArray[i+1] = temp;
  //         flag = true;
  //       }
  //     }
  //   }
  //   return sortedArray;
  // }

  // public static int[] getIntegers(int num) {
  //   System.out.println("Enter " + num + " inter values.\r");
  //   int[] values = new int[num];

  //   for(int i = 0; i < values.length; i++){
  //     values[i] = scanner.nextInt();
  //   }
  //   return values;
  // }

  // public static void printArray(int [] array) {
  //   for (int i = 0; i < array.length; i++) {
  //     System.out.println("Element " + i + ", value is " + array[i]);
  //   }
  // }

  // public static double getAverage(int[] array) {
  //   int sum = 0;
  //   for (int i = 0; i < array.length; i++) {
  //     sum += array[i];
  //   }
  //   return (double) sum/(double) array.length;
  // }

}
