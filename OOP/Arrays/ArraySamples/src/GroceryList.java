import java.util.ArrayList;

public class GroceryList {

  private ArrayList<String> groceryList = new ArrayList<String>();

  public void addGroceryItem(String item) {
    groceryList.add(item);
  }

  public void printGroceryList() {
    for (int i = 0; i < groceryList.size(); i++) {
      System.out.println((i+1) + ". " + groceryList.get(i));
    }
  }

  public void modifyGroceryList(String oldItem, String newItem) {
    int pos = findItem(oldItem);
    if (pos >= 0) {
      modifyGroceryList(pos, newItem);
    }
  }

  private void modifyGroceryList(int position, String newItem) {
    groceryList.set(position, newItem);
  }

  private void removeGroceryListItem(int position) {
    groceryList.remove(position);
  }

  public void removeGroceryListItem(String item) {
    int pos = findItem(item);
    if (pos >= 0) {
      removeGroceryListItem(pos);
    }
  }

  public int findItem(String searchItem) {
    return groceryList.indexOf(searchItem);
    // boolean exists = groceryList.contains(searchItem);
    // int pos = groceryList.indexOf(searchItem);
    // if (pos >= 0) {
    //   return groceryList.get(pos);
    // }

    // return null;
  }

  public ArrayList<String> getGroceryList() {
    return groceryList;
  }

  public void setGroceryList(ArrayList<String> groceryList) {
    this.groceryList = groceryList;
  }
}


