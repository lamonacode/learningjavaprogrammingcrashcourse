
/**
 * HealthyBurger
 */
public class HealthyBurger extends BaseBurger {

  public HealthyBurger() {
    super(new Meat("Veggie", 3.25), new Bun("Brown Rye", 2.50), "Healthy Burger", 6);
  }
}
