
/**
 * BaseBurger
 */
public class BaseBurger {
  private String name;
  private int numAddOns;
  private Meat meatType;
  private Bun bunType;

  private String itemList;
  private float cost;


  public BaseBurger() {
    this(new Meat("Beef", 2.25), new Bun("Hard Roll", 2), "Basic Burger", 4);
  }

  public BaseBurger(Meat meatType, Bun bunType, String name, int addOns) {
    this.meatType = meatType;
    this.bunType = bunType;
    this.name = name;

    this.itemList = "";
    this.cost = 0;
    this.numAddOns = addOns + 2;

    addItem(meatType.getName(), meatType.getCost());
    addItem(bunType.getName(), bunType.getCost());

  }


  public void addItem(String name, double amount) {
    if (numAddOns > 0) {
      numAddOns -= 1;
      itemList += String.format("%1$20s", name) + String.format("%1$20s", amount) + "\n";
      cost += amount;
    } else {
      System.out.println("Unable to add " + name);
    }
  }

  public void getTotal(){
    System.out.println(getClass().getSimpleName());

    System.out.println(itemList);
    String.format("%40s", "-").replace(' ', '-');
    System.out.println(String.format("%40s", cost));
  }

  public int getNumAddOns() {
    return numAddOns;
  }

  public void setNumAddOns(int numAddOns) {
    this.numAddOns = numAddOns;
  }

  public Meat getMeatType() {
    return meatType;
  }

  public void setMeatType(Meat meatType) {
    this.meatType = meatType;
  }

  public Bun getBunType() {
    return bunType;
  }

  public void setBunType(Bun bunType) {
    this.bunType = bunType;
  }

  public String getItemList() {
    return itemList;
  }

  public void setItemList(String itemList) {
    this.itemList = itemList;
  }

  public float getCost() {
    return cost;
  }

  public void setCost(float cost) {
    this.cost = cost;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


}
