
/**
 * Bun
 */
public class Bun {
  private String name;
  private double cost;

  public Bun(String name, double cost) {
    this.name = name;
    this.cost = cost;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getCost() {
    return cost;
  }

  public void setCost(float cost) {
    this.cost = cost;
  }
}
