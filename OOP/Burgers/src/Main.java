/**
 * Main
 */
public class Main {

  public static void main(String[] args) {
    BaseBurger burger1 = new BaseBurger();
    burger1.addItem("Lettuce", .75);
    burger1.addItem("Cheese", .75);
    burger1.addItem("Ketchup", 0.0);
    // burger1.getTotal();
    burger1.addItem("Hot Sauce", 1.25);
    burger1.addItem("Mustard", 0.0);
    burger1.addItem("Pickles", .25);
    burger1.getTotal();

    HealthyBurger burger2 = new HealthyBurger();
    burger2.addItem("Lettuce", .75);
    burger2.addItem("Cheese", .75);
    burger2.addItem("Ketchup", 0.0);
    // burger2.getTotal();
    burger2.addItem("Hot Sauce", 1.25);
    burger2.addItem("Mustard", 0.0);
    burger2.addItem("Pickles", .25);
    burger2.addItem("Onions", .25);
    burger2.addItem("Relish", .25);
    burger2.addItem("Tomato", .25);
    burger2.getTotal();

    // DeluxeBurger burger3 = new DeluxeBurger(new Meat("Veggie", 3.25), new Bun("Brown Rye", 2.50));
    DeluxeBurger burger3 = new DeluxeBurger();
    burger3.addItem("Lettuce", .75);
    burger3.addItem("Cheese", .75);
    burger3.addItem("Ketchup", 0.0);
    burger3.getTotal();
  }



}
