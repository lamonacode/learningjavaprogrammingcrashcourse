
/**
 * DeluxeBurger
 */
public class DeluxeBurger extends BaseBurger{

  public DeluxeBurger() {
    super(new Meat("Beef", 2.25), new Bun("Hard Roll", 2), "Deluxe Burger", 2);
    this.addItem("Chips", 1.75);
    this.addItem("Drank", 1.50);
  }

  public DeluxeBurger(Meat meatType, Bun bunType) {
    super(meatType, bunType, "Custom Deluxe Burger", 2);
    this.addItem("Chips", 1.75);
    this.addItem("Drank", 1.50);
  }


}
