package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {

        String first = "This is a String";
        String second = new String("This is a String"); // this avoids the string object pool referencing
        String third = "THIS IS A STRING";

        System.out.println(first == second);
        System.out.println(second == first);
        System.out.println(first.equals(second));
        System.out.println(second.equals(first));
        System.out.println(second.equalsIgnoreCase(third));
    }
}
