package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {

        StringBuilder first = new StringBuilder("This is a string");
        StringBuilder second = new StringBuilder("This is a string");

        System.out.printf("first: %s %n", first);
        System.out.println("clearing first");
        if (first.delete(0, first.length()) == first) {
            System.out.println("the messages are the same reference");
        }
        System.out.printf("first *%s* %n", first);

        first.append("another string.");
        System.out.printf("first *%s* %n", first);

        first.delete(0, first.length()).append("another, another string"); // method chaining
        System.out.printf("first *%s* %n", first);


//        String firstString = first.toString();
//        String secondString = second.toString();

//        System.out.printf("first: %s %n", first);
//        System.out.printf("second: %s %n", second);
//        System.out.printf("first is the same as second: %s %n", first == second);
//
//        System.out.println();

//        first.replace(" ", "_");
//        first.replace(4, 5, "_");
//        System.out.printf("first: %s %n", first);
//        System.out.printf("second: %s %n", second);
//        System.out.printf("first is the same as second: %s %n", first == second); // reference equality
//        System.out.printf("first.equals(second): %s %n", first.equals(second)); // value equality
//        System.out.println();
//
//        System.out.printf("firstString: %s %n", firstString);
//        System.out.printf("secondString: %s %n", secondString);
//        System.out.printf("firstString is the same as secondString: %s %n", firstString == secondString); // reference equality
//        System.out.printf("firstString.equals(secondString): %s %n", firstString.equals(secondString)); // value equality
//        System.out.println();

//        first = first.replace(" ", "_");
//        first = first.replace("_", " ");
//        System.out.printf("first: %s %n", first);
//        System.out.printf("second: %s %n", second);
//        System.out.printf("first is the same as second: %s %n", first == second); // reference equality
//        System.out.printf("first is the same as second: %s %n", first.equals(second)); // value equality


    }
}
