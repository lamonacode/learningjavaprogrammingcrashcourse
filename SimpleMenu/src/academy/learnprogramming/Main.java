package academy.learnprogramming;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String userChoice = " ";
        printMenu();

        while (!userChoice.equalsIgnoreCase("q")) {

            userChoice = scanner.nextLine();

            switch (userChoice) {
                case "1":
                    System.out.println("Making Cappucino");
                    break;
                case "2":
                    System.out.println("Making Latte");
                    break;
                case "3":
                    System.out.println("Making Americano");
                    break;
                case "4":
                    System.out.println("Making Mocha");
                    break;
                case "5":
                    System.out.println("Making Macchiato");
                    break;
                case "6":
                    System.out.println("Making Espresso");
                    break;
                case "q":
                    System.out.println("Goodbye.");
                    break;
                default:
                    System.out.println("Invalid selection. Please try again");
                    continue;
            }
            System.out.println("Dispensing coffee");
            System.out.println("Have a nice day.");
        }

        scanner.close();
    }

    private static void printMenu() {
        System.out.println("Please choose one of the following options");
        System.out.println("1 - Cappucino");
        System.out.println("2 - Latte");
        System.out.println("3 - Americano");
        System.out.println("4 - Mocha");
        System.out.println("5 - Macchiato");
        System.out.println("6 - Espresso");
        System.out.println("Q - Quit the program");
    }
}
