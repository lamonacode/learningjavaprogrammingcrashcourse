package app;

public class App {
    public static void main(String[] args) throws Exception {
      displayHighScorePosition("Jeff", calculateHighScorePosition(1500));
      displayHighScorePosition("Bonnie", calculateHighScorePosition(900));
      displayHighScorePosition("Sam", calculateHighScorePosition(400));
      displayHighScorePosition("Alex", calculateHighScorePosition(50));
    }

    public static void displayHighScorePosition (String playerName, int position) {
      System.out.println(playerName + " managed to get into position " + position + " on the high score table");
    }

    public static int calculateHighScorePosition (int score) {
      if (score >= 1000) {
        return 1;
      } else if ((score >= 500) && (score < 1000)) {
        return 2; 
      } else if ((score >= 100) && (score < 500)) {
        return 3; 
      } else return 4;

    }
}