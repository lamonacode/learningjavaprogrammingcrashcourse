package academy.lamonacode;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) throws Exception {

      // these types are available, but use defaults below instead... 
      byte myByteVar = 100;
      short myShortVar = 200;      
      long myLongVar = 50000L + 10L * (myByteVar + myShortVar + 500);
      float myFloatVar = 5F / 3F; // float is not default, assumes double; can either cast (float) or use the 'F'      
      char myChar = 'D';
      char myUnicode = '\u00A9' + 5;

      // these are the main (default) data types to use in Java... 
      boolean myTrue = true;
      int myIntVar = 300 / 3;
      double myDoubleVar = 5D / 3D; // double is default; 'D' is not necessary

      // BigDecimal is used for currency... 
      BigDecimal myBigDecimal = new BigDecimal(100.100);
      
      System.out.println("myLongVar: " + myLongVar);
      System.out.println("myIntVar: " + myIntVar);
      System.out.println("myFloatVar: " + myFloatVar);
      System.out.println("myDoubleVar: " + myDoubleVar);
      System.out.println(myChar);
      System.out.println(myUnicode);
      System.out.println(myTrue);
      System.out.println(myBigDecimal);

      String myString = "this is my string";
      System.out.println("myString = " + myString);
      myString = myString + ", and here is more";
      System.out.println("myString = " + myString);
      myString = myString + " \u00A92019";
      System.out.println("myString = " + myString);

      String myIntString = "10";
      myIntString = myIntString + myIntVar;
      System.out.println("myIntString = " + myIntString); // will append the int as string; 10100


    }
}