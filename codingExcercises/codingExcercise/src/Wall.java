/**
 * Wall
 */
public class Wall {

  private String name;
  private String color;
  private boolean hasDoor;
  private boolean hasWindow;

  public Wall(String name, String color, boolean hasDoor, boolean hasWindow) {
    this.name = name;
    this.color = color;
    this.hasDoor = hasDoor;
    this.hasWindow = hasWindow;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public boolean isHasDoor() {
    return hasDoor;
  }

  public void setHasDoor(boolean hasDoor) {
    this.hasDoor = hasDoor;
  }

  public boolean isHasWindow() {
    return hasWindow;
  }

  public void setHasWindow(boolean hasWindow) {
    this.hasWindow = hasWindow;
  }
}