

/**
 * VIPCustomer
 */
public class VIPCustomer {

  private String name;
  private double creditLimit;
  private String emailAddress;

  public VIPCustomer() {
    this.name = "Default Name";
    this.creditLimit = 0;
    this.emailAddress = "Default Email";
  }

  public VIPCustomer(String name, double creditLimit) {
    this(name, creditLimit, "Default Email");
  }

  public VIPCustomer(String name, double creditLimit, String emailAddress) {
    this.name = name;
    this.creditLimit = creditLimit;
    this.emailAddress = emailAddress;
  }

  public String getName() {
    return name;
  }

  public double getCreditLimit() {
    return creditLimit;
  }

  public String getEmailAddress() {
    return emailAddress;
  }
  
}