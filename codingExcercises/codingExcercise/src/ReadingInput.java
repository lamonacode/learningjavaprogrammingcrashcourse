import java.util.Scanner;

/**
 * ReadingInput
 */
public class ReadingInput {

  public static void ReadUserInput(){
    Scanner scanner = new Scanner(System.in);

    System.out.println("Please enter your birth year: ");
    int birthYear = scanner.nextInt();
    scanner.nextLine();

    System.out.println("Please enter your name: ");
    String name = scanner.nextLine();

    System.out.println(name + " is " + (2019 - birthYear) + " years old.");
    scanner.close();
  }
}