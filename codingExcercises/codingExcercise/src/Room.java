/**
 * Room
 */
public class Room {

  private String name;
  private Wall wall1;
  private Wall wall2;
  private Wall wall3;
  private Wall wall4;
  private Light light1;
  private Light light2;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Room(String name, Wall wall1, Wall wall2, Wall wall3, Wall wall4, Light light1, Light light2) {
    this.name = name;
    this.wall1 = wall1;
    this.wall2 = wall2;
    this.wall3 = wall3;
    this.wall4 = wall4;
    this.light1 = light1;
    this.light2 = light2;
  }

  public void turnOnLights(){
    light1.setState(true);
    light2.setState(true);
    System.out.println("lights on..");
    checkLights();
  }

  public void turnOffLights(){
    light1.setState(false);
    light2.setState(false);
    System.out.println("lights off..");
    checkLights();    
  }

  public void checkLights() {
    System.out.println("light1: " + light1.getState());
    System.out.println("light2: " + light2.getState());
  }

  public Wall getWall1() {
    return wall1;
  }

  public void setWall1(Wall wall1) {
    this.wall1 = wall1;
  }

  public Wall getWall2() {
    return wall2;
  }

  public void setWall2(Wall wall2) {
    this.wall2 = wall2;
  }

  public Wall getWall3() {
    return wall3;
  }

  public void setWall3(Wall wall3) {
    this.wall3 = wall3;
  }

  public Wall getWall4() {
    return wall4;
  }

  public void setWall4(Wall wall4) {
    this.wall4 = wall4;
  }
  
}