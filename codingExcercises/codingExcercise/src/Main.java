

  /**
   * Car
   */
  class Car {

    private String name;
    private boolean engine;
    private int cylinders;
    private int wheels;

    public Car(String name, int cylinders) {
      this.name = name;
      this.cylinders = cylinders;
      this.engine = true;
      this.wheels = 4;
    }

    public String startEngine() {
      if (engine) {
        return "Car: Starting engine...";
      } else {
        return "Car: woops. no engine here?";
      }
    }

    public String accelerate() {
      return "Car: Accelerating...";
    }

    public String brake() {
      return "Car: Braking...";
    }

    public String getName() {
      return name;
    }

    public int getCylinders() {
      return cylinders;
    }

    public void setEngine(boolean value) {
      this.engine = value;
    }

  public int getWheels() {
    return wheels;
  }

  public void setWheels(int wheels) {
    this.wheels = wheels;
  }
  }

  /**
   * Vue extends Car
   */
  class Vue extends Car {

    public Vue() {
      super("Vue", 4);
    }

    @Override
    public String accelerate() {
      return "Vue goes put put put...";
    }

  }

  class Traverse extends Car {
    public Traverse() {
      super("Traverse", 6);
    }

    @Override
    public String brake() {
      return getClass().getSimpleName() +  " needs new brakes!!! probably.";
    }
  }

  /**
   * MatchboxCar
   */
  class MatchboxCar extends Car{

    public MatchboxCar() {
      super("MatchboxCar", 0);
      super.setEngine(false);
    }

  }


/**
 * Main
 */
public class Main {
  public static void main(String[] args) {

    // Car car = new Car("Test", 8);
    // System.out.println(car.startEngine());
    // System.out.println(car.accelerate());
    // System.out.println(car.brake());

    // Traverse traverse = new Traverse();
    // System.out.println(traverse.startEngine());
    // System.out.println(traverse.accelerate());
    // System.out.println(traverse.brake());

    // Printer printer = new Printer(50, false);
    // System.out.println("Pages printed = " + printer.getNumPagesPrinted());
    // printer.print(4);
    // System.out.println("Pages printed = " + printer.getNumPagesPrinted());
    // printer.print(3);
    // System.out.println("Pages printed = " + printer.getNumPagesPrinted());
    // printer.print(1);
    // System.out.println("Pages printed = " + printer.getNumPagesPrinted());

    // Wall eWall = new Wall("East", "Blue", false, true);
    // Wall wWall = new Wall("West", "Blue", false, false);
    // Wall nWall = new Wall("North", "White", true, false);
    // Wall sWall = new Wall("South", "Blue", false, false);

    // Light light1 = new Light("ceiling", false);
    // Light light2 = new Light("table", false);

    // Room livingRoom = new Room("living room", eWall, wWall, nWall, sWall, light1, light2);

    // livingRoom.checkLights();
    // livingRoom.turnOnLights();
    // light2.setState(false);
    // livingRoom.checkLights();


    // Circle circle = new Circle(3.75);
    // System.out.println("C rad = " + circle.getRadius());
    // System.out.println("C area = " + circle.getArea());
    // Cylinder cylinder = new Cylinder(5.55, 7.25);
    // System.out.println("cy rad = " + cylinder.getRadius());
    // System.out.println("cy height = " + cylinder.getHeight());
    // System.out.println("cy area = " + cylinder.getArea());
    // System.out.println("cy vol = " + cylinder.getVolume());


    // Traverse traverse = new Traverse("Jeff");
    // traverse.steer(45);
    // traverse.accelerate(30);
    // traverse.accelerate(23);
    // traverse.accelerate(-53);


    // Animal animal = new Animal("Animal", 1, 1, 5, 5);
    // Dog dog = new Dog("Yorkie", 8, 20, 2, 4);
    // // Dog dog2 = new Dog("Poodle", 1, 2, 2, 4, 1, 20, "curly");
    // animal.eat();
    // dog.eat();
    // dog2.walk();
    // dog2.run();

    // VIPCustomer vipCustomer = new VIPCustomer();
    // System.out.println(vipCustomer.getName() + " " + vipCustomer.getCreditLimit()
    // + " " + vipCustomer.getEmailAddress());

    // VIPCustomer vipCustomer1 = new VIPCustomer("Jeff", 10000.00);
    // System.out.println(vipCustomer1.getName() + " " +
    // vipCustomer1.getCreditLimit() + " " + vipCustomer1.getEmailAddress());

    // VIPCustomer vipCustomer2 = new VIPCustomer("Other", 6.05, "email.com");
    // System.out.println(vipCustomer2.getName() + " " +
    // vipCustomer2.getCreditLimit() + " " + vipCustomer2.getEmailAddress());

    // BankClasses myAccount = new BankClasses();

    // myAccount.setAccountNumber(1234567890);
    // myAccount.setBalance(100);
    // myAccount.setCustomerName("Jeff LaMonaco");
    // myAccount.setEmail("jeff@google.com");
    // myAccount.setPhoneNumber("898.498.4400");

    // System.out.println(myAccount.getCustomerName());
    // System.out.println(myAccount.getAccountNumber());
    // System.out.println(myAccount.getEmail());
    // System.out.println(myAccount.getPhoneNumber());
    // System.out.println(myAccount.getBalance());

    // myAccount.deposit(50.50);
    // myAccount.withdraw(25.30);
    // myAccount.withdraw(225.30);

    // BankClasses yourAccount = new BankClasses(99999999, 1234.56, "you",
    // "me.gmail.com", "234.234.2222");
    // System.out.println(yourAccount.getCustomerName());
    // System.out.println(yourAccount.getAccountNumber());
    // System.out.println(yourAccount.getEmail());
    // System.out.println(yourAccount.getPhoneNumber());
    // System.out.println(yourAccount.getBalance());

  }
}
