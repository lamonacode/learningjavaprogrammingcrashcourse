/**
 * Light
 */
public class Light {

  private String name;
  private boolean state;

  public Light(String name, boolean state) {
    this.name = name;
    this.state = state;
  }

  public void flipSwitch() {
    this.setState(!this.getState());
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean getState() {
    return state;
  }

  public void setState(boolean state) {
    this.state = state;
  }
}