/**
 * SpeedConverter
 */
public class SpeedConverter {
  public static long toMilesPerHour (double kilometersPerHour) {
    if(kilometersPerHour < 0) { 
      return -1L;
    }

    return Math.round(kilometersPerHour / 1.609);
  }

  public static void printConversion(double kilometersPerHour) {
    if (kilometersPerHour < 0) {
      System.out.println("Invalid Value");
    }
    else {
      long milesPerHour = toMilesPerHour(kilometersPerHour);
      System.out.println(kilometersPerHour + " km/h = " + milesPerHour + " mi/h");
    }
  }

  public static void printMegaBytesAndKiloBytes(int kiloBytes) {
    if (kiloBytes < 0)
      System.out.println("Invalid Value");
    int megaBytes = Math.abs(kiloBytes/1024);
    int kBs =  kiloBytes % 1024;

    System.out.println(kiloBytes + " KB = " + megaBytes + " MB and " + kBs + " KB");
  }


  public static boolean areEqualByThreeDecimalPlaces(double num1, double num2) {
    if ((int)(num1 * 1000) == (int)(num2 * 1000)) {
      return true;
    } 
    return false;
  }

}