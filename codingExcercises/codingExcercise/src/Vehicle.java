/**
 * Vehicle
 */
public class Vehicle {
  private String name;
  private int size;
  private int currentDirection;
  private int currentVelocity;


  public Vehicle(String name, int size) {
    this.name = name;
    this.size = size;
    this.currentDirection = 0;
    this.currentVelocity = 0;
  }

  public void steer(int direction) {
    this.currentDirection += direction;
    System.out.println("Vehicle.steer(). Steering at " + currentDirection);
  }

  public void stop() {
    System.out.println("Vehicle.stop(): stopping.");
    this.currentVelocity = 0;
  }

  public void move(int velo, int dir){
    currentDirection = dir;
    currentVelocity = velo;
    System.out.println("Vehicle.move(). Steering at " + currentDirection + "; Velocity at " + currentVelocity);
  }
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public int getCurrentDirection() {
    return currentDirection;
  }

  public void setCurrentDirection(int currentDirection) {
    this.currentDirection = currentDirection;
  }

  public int getCurrentVelocity() {
    return currentVelocity;
  }

  public void setCurrentVelocity(int currentVelocity) {
    this.currentVelocity = currentVelocity;
  }
}