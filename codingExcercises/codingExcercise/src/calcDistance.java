/**
 * calcDistance
 */
public class calcDistance {

  
  public static double calcFeetAndInches(int feet, int inches) {
    if ((feet < 0) || (inches < 0) || (inches > 12)) {
      return -1d;
    }

    int totalInches = (feet * 12) + inches;
    return calcFeetAndInches(totalInches);
  }

  public static double calcFeetAndInches(int inches) {
    return inches * 2.54;
  }
}