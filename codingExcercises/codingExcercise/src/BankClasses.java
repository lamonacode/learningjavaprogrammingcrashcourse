/**
 * BankClasses
 */
public class BankClasses {

  private int accountNumber;
  private double balance;
  private String customerName;
  private String email;
  private String phoneNumber;

  public int getAccountNumber(){
    return this.accountNumber;
  }
  public void setAccountNumber(int val){
    this.accountNumber = val;
  }
  public double getBalance(){
    return this.balance;
  }
  public void setBalance(double val){
    this.balance = val;
  }
  public String getCustomerName(){
    return this.customerName;
  }
  public void setCustomerName(String val){
    this.customerName = val;
  }
  public String getEmail(){
    return this.email;
  }
  public void setEmail(String val){
    this.email = val;
  }
  public String getPhoneNumber(){
    return this.phoneNumber;
  }
  public void setPhoneNumber(String val){
    this.phoneNumber = val;
  }


  public void deposit(double val) {
    this.balance += val;
    System.out.println("Current balance is: " + this.balance);
  }

  public void withdraw(double val) {
    if (val > this.balance) {
      System.out.println("Insufficient funds; Current balance is: " + this.balance);
      return;
    } else {
      this.balance -= val;
      System.out.println("Current balance is: " + this.balance);
    }
  }

  public BankClasses(){
    // can set defaults here by calling param constructor
    System.out.println("empty constructor");
  }
  public BankClasses(int number, double balance, String name, String email, String phone){
    this.accountNumber = number;
    this.balance = balance;
    this.customerName = name;
    this.email = email;
    this.phoneNumber = phone;
    System.out.println("full param constructor");
  }

}