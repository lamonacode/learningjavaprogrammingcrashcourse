/**
 * Switch
 */
public class Switch {

  // public static void printDayOfTheWeek(int day) {

  //   switch(day) {
  //     case 0: 
  //       System.out.println("Sunday");
  //       break;
  //     case 1: 
  //       System.out.println("Monday");
  //       break;
  //     case 2: 
  //       System.out.println("Tuesday");
  //       break;
  //     case 3: 
  //       System.out.println("Wednesday");
  //       break;
  //     case 4: 
  //       System.out.println("Thursday");
  //       break;
  //     case 5: 
  //       System.out.println("Friday");
  //       break;
  //     case 6: 
  //       System.out.println("Saturday");
  //       break;
  //     default: 
  //       System.out.println("Invalid day, please enter 0 thru 7");
  //       break;
  //   }

  //   if (day == 0) {
  //     System.out.println("Sunday");
  //   } else if (day == 1) {
  //     System.out.println("Monday");
  //   } else if (day == 2) {
  //     System.out.println("Tuesday");
  //   } else if (day == 3) {
  //     System.out.println("Wednesday");
  //   } else if (day == 4) {
  //     System.out.println("Thursday");
  //   } else if (day == 5) {
  //     System.out.println("Friday");
  //   } else if (day == 6) {
  //     System.out.println("Saturday");
  //   } else {
  //     System.out.println("Invalid day, please enter 0 thru 7");
  //   }
  // }

  // public static void doSwitch() {
  //   int value = 1;

  //   if (value == 1) {
  //     System.out.println("value is 1");
  //   } else if (value == 2) {
  //     System.out.println("value is 2");
  //   } else {
  //     System.out.println("unknown value");
  //   }

  //   int switchValue = 2;

  //   switch(switchValue) {
  //     case 1: 
  //       System.out.println("value is 1");
  //       break;
  //     case 2: 
  //       System.out.println("value is 2");
  //       break;
  //     default: 
  //       System.out.println("unknown value");
  //       break;
  //   }

  //   char charValue = 'D';
  //   switch(charValue) {
  //     case 'A': 
  //       System.out.println("value is A");
  //       break;
  //     case 'B': 
  //       System.out.println("value is B");
  //       break;
  //     case 'C': case 'D': case 'E':
  //       System.out.println("value is C, D, or E");
  //       break;
  //     default: 
  //       System.out.println("unknown value");
  //       break;
  //   }

  //   String month = "JanUAry";
  //   switch (month.toLowerCase()) {
  //     case "january": 
  //       System.out.println("Jan");
  //       break;
  //     default: 
  //       System.out.println("unknown value");
  //       break;
  //   }
  // }


    public static boolean isLeapYear(int year){
        if (year < 1 || year > 9999) {
            return false;
        } else if (year % 400 == 0) {
            return true;
        } else if ((year % 4 == 0) && (year % 100 != 0)) {
            return true;
        } else {
            return false;
        }
    }
    
    public static int getDaysInMonth(int month, int year) {
      if ((month < 1) || (month > 12)) {
          return -1;
      } 
      if (year < 1 || year > 9999) {
          return -1;
      }
      
      switch(month) {
      case 1: case 3: case 5: case 7: case 8: case 10: case 12:  
          return 31;
      case 2: 
        if (isLeapYear(year)) {
            return 29;
        } else {
            return 28;
        }
      case 4: case 6: case 9: case 11: 
        return 30;
      default: 
        return -1;
    }
  }

}