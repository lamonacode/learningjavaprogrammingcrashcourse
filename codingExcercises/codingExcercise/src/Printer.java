/**
 * Printer
 */
public class Printer {

  private int tonerLevel;
  private int pagesPrinted;
  private boolean isDuplex;

  public Printer(int tonerLevel, boolean isDuplex) {
    if (tonerLevel >=0 && tonerLevel <= 100) {
    this.tonerLevel = tonerLevel;
    } else {
      tonerLevel = 0;
    }
    this.pagesPrinted = 0;
    this.isDuplex = isDuplex;
  }

  public void fillToner(int amount) {
    tonerLevel += amount;
    if (tonerLevel > 100) {
      tonerLevel = 100;
    }
    if (tonerLevel < 0 ) {
      tonerLevel = 0;
    }
  }

  public void printPage() {
    pagesPrinted += 1;
  }

  public int print(int pages) {
    int pagesToPrint = pages;
    if (isDuplex) {
      pagesToPrint = (pages/2) + (pages %2);
    }
    for (int i = 0; i < pagesToPrint; i++) {
      printPage();
    }
    return pagesToPrint;
  }

  public int getTonerLevel() {
    return tonerLevel;
  }

  public void setTonerLevel(int tonerLevel) {
    this.tonerLevel = tonerLevel;
  }

  public int getNumPagesPrinted() {
    return pagesPrinted;
  }

  public void setNumPagesPrinted(int numPagesPrinted) {
    this.pagesPrinted = numPagesPrinted;
  }

  public boolean isDuplex() {
    return isDuplex;
  }

  public void setDuplex(boolean isDuplex) {
    this.isDuplex = isDuplex;
  }
}