
public class SecondsAndMinutesChallenge
 {

  public static void main (String[] args) {
    System.out.println(getDurationString(61, 0));
    System.out.println(getDurationString(59, 59));
    System.out.println(getDurationString(61));
    System.out.println(getDurationString(3601));

    System.out.println(getDurationString(65, 45));
    System.out.println(getDurationString(3945));
    
  }

  public static String getDurationString(int minutes, int seconds) {
    if ((minutes < 0) || (seconds < 0) || (seconds > 59)) {
      return "Invalid value";
    } else {      
      int tSeconds = (seconds % 60);
      int tMinutes = (int)(seconds/60); // first see if > 60 seconds passed in
      tMinutes += (minutes % 60);
      int tHours = (int)(minutes/60);

      return tHours + "h " + tMinutes + "m " + tSeconds + "s";
    }
  }

  public static String getDurationString(int seconds) {
    if (seconds < 0 ) {
      return "Invalid value";

    } else {
      int tSeconds = (seconds % 60);
      int tMinutes = (int)(seconds/60); // first see if > 60 seconds passed in
      
      return getDurationString(tMinutes, tSeconds);
    }
  }

  public static void printEqual(int num1, int num2, int num3) {
    if ((num1 < 0) || (num2 < 0) || (num3 < 0 )) {
        System.out.println("Invalid Value");
    } else if ((num1 == num2) && (num2 == num3)) {
        System.out.println("All numbers are equal");
    } else if ((num1 != num2) && (num1 != num3) && (num2 != num3)) {
        System.out.println("All numbers are different");
    } else {
        System.out.println("Neither all are equal or different");
    }
    
}

}