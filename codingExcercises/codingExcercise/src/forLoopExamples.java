/**
 * forLoopExamples
 */
public class forLoopExamples {

  public static void doLoops(int int1, int int2) {
    int low = int1;
    int high = int2;

    if (int1 > int2) { 
      for (int i = low; i >= high; i --) {
        System.out.println("10,000 at " + i + " % interest = " + String.format("%.2f", calculateInterest(10000.00, i)));
      }
    } else {
      for (int i = low; i <= high; i ++) {
        System.out.println("10,000 at " + i + " % interest = " + String.format("%.2f", calculateInterest(10000.00, i)));
      }
    }
  }

  public static double calculateInterest(double amount, double interestRate) {
    return (amount * (interestRate/100));
  }




}