/**
 * classes
 */
public class Cars extends Vehicle {
  private int doors;
  private int wheels;
  private int gears;
  private boolean isManual;
  private int currentGear;

  public Cars(String name, int size, int doors, int wheels, int gears, boolean isManual) {
    super(name, size);
    this.doors = doors;
    this.wheels = wheels;
    this.gears = gears;
    this.isManual = isManual;
    this.currentGear = 1;
  }

  public int getDoors() {
    return doors;
  }

  public void setDoors(int doors) {
    this.doors = doors;
  }

  public int getWheels() {
    return wheels;
  }

  public void setWheels(int wheels) {
    this.wheels = wheels;
  }

  public int getGears() {
    return gears;
  }

  public void setGears(int gears) {
    this.gears = gears;
  }

  public boolean isManual() {
    return isManual;
  }

  public void setManual(boolean isManual) {
    this.isManual = isManual;
  }

  public int getCurrentGear() {
    return currentGear;
  }

  public void setCurrentGear(int currentGear) {
    this.currentGear = currentGear;
    System.out.println("Car.setCurrentGear(): changed to " + this.currentGear + " gear.");
  }

  public void changeVelocity(int speed, int direction) {
    move(speed, direction);
    System.out.println("Car.changeVelocity(): Velocity = " + speed + " direction " + direction);
  }
}
