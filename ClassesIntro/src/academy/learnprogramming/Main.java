package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {

        Car myCar = new Car("Jeff's car");
        Car anotherCar = new Car("The Batmobile");

        myCar.accelerate(3);
        myCar.accelerate(4);
        myCar.accelerate(5);
        myCar.accelerate(6);
        myCar.brake(4);

        anotherCar.accelerate(4);
        anotherCar.accelerate(20);

        myCar.accelerate(2);
        myCar.accelerate(2);
        myCar.accelerate(1);

        anotherCar.brake(40);
    }
}

class Car {

    private int speed = 0;
    private String name;

    public Car(String carName){
        name = carName;
    }

    public void accelerate(int aSpeed){
        speed += aSpeed;
        showSpeed();
    }

    public void brake(int aSpeed){
        // still dont like ternary
//        speed = (speed < aSpeed) ? 0 : speed - aSpeed;

        speed -= aSpeed;
        if (speed < 0) {
            speed = 0;
        }
        showSpeed();
    }

    private void showSpeed(){
        System.out.printf("%s is going %d mph.%n", name, speed);
    }
}
